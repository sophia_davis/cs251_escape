/* Interface for escaping and un-escaping strings */

/* This function replaces special characters in the string with their
   escaped two-character versions.

   This function allocates and returns a string, which must be freed by
   the caller.
 */
char *escape(char *unescaped);

/* This function replaces escaped characters with their one-character
   equivalents.

   This function allocates and returns a string, which must be freed by
   the caller.
*/
char *unescape(char *escaped);
