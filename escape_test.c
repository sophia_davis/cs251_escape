/*
escape_test.c; cs251_escape
Sophia Davis
A file testing string escape and unescape functions implemented in escape.c.
due 2/3/2014
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "escape.h"

int main() {

  char *hello = "Hello\n\n\n\\world";
  char *hello_escaped = escape(hello);
  char *hello_escaped_unescaped = unescape(hello_escaped);
  
  printf("regular: %s\n", hello);
  printf("escaped: %s\n", hello_escaped);
  printf("unescaped: %s\n", hello_escaped_unescaped);
  
  assert(strcmp(hello, hello_escaped_unescaped) == 0);
  
  free(hello_escaped);
  free(hello_escaped_unescaped);
  
  printf("-----------------------\n");
  
  char *s = "My\tname\tis\tDave";
  char *s_escaped = escape(s);
  char *s_escaped_unescaped = unescape(s_escaped);
  
  printf("regular: %s\n", s);
  printf("escaped: %s\n", s_escaped); // prints:   My\tname\tis\tDave
  printf("unescaped: %s\n", s_escaped_unescaped);
  
  assert(strcmp(s, s_escaped_unescaped) == 0);
  
  free(s_escaped);
  free(s_escaped_unescaped);
  
  printf("-----------------------\n");
  
  char *testall = "Hello\n\\world\t\'\"\\";
  char *testall_escaped = escape(testall);
  char *testall_escaped_unescaped = unescape(testall_escaped);
  
  printf("regular: %s\n", testall);
  printf("escaped: %s\n\n", testall_escaped);
  printf("unescaped: %s\n", testall_escaped_unescaped);
  
  assert(strcmp(testall, testall_escaped_unescaped) == 0);

  free(testall_escaped);
  free(testall_escaped_unescaped);
  
  printf("-----------------------\n");
  
  char unescape_test[] = {'H', 'e', 'l', 'l', 'o', '\\', 'n', 'w', 'o', 'r', 'l', 'd', '\0'}; // "Hello\\nworld"
  char *unescape_test_result = unescape(unescape_test);
  printf("regular: %s\n", unescape_test);
  printf("unescaped: %s\n", unescape_test_result);
  
  free(unescape_test_result);
  
  printf("-----------------------\n");
  
  char *escape_test = escape("hello\nworld");
  printf("regular: hello\nworld\n");
  printf("escaped: %s\n", escape_test);
  
  free(escape_test);
  
  printf("-----------------------\n");
  
  char *file = "\'\"images\\foo.jpg\"\'";
  char *file_escaped = escape(file);
  char *file_escaped_unescaped = unescape(file_escaped);

  printf("regular: %s\n", file);
  printf("escaped: %s\n", file_escaped);
  printf("unescaped: %s\n", file_escaped_unescaped);

  assert(strcmp(file, file_escaped_unescaped) == 0);
  
  free(file_escaped);
  free(file_escaped_unescaped); 
  
  printf("-----------------------\n");
}

