/*
escape_test.c; cs251_escape
Sophia Davis
A file implementing string escape and unescape functions.
due 2/3/2014
*/

/* Implementation of the escape and unescape functions */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "escape.h"

/* This method replaces special characters in the string with their
   escaped two-character versions. */
char *escape(char *unescaped) {
  
  int toMalloc = 0;
  int length = strlen(unescaped);
    
  int i;
  char letter;
  
  for (i = 0; i < length; i++) { // unescaped[length] will be null
    letter = unescaped[i];
    
    if (letter == '\n' ||
        letter == '\t' ||
        letter == '\\' ||
        letter == '\'' ||
        letter == '\"') {
      toMalloc = toMalloc + 2;
    }
    else {
      toMalloc ++;
    }
  }
  toMalloc ++; // add space for null
  
  char *escaped;
  escaped = malloc(toMalloc*sizeof(char));
  int nextFreeIndex = 0;
  
  for (i = 0; i < length; i++) {
    letter = unescaped[i];
    
    switch(letter) {  
      case '\n':
        escaped[nextFreeIndex] = '\\';
        escaped[nextFreeIndex + 1] = 'n';
        nextFreeIndex = nextFreeIndex + 2;
        break;
      case '\t':
        escaped[nextFreeIndex] = '\\';
        escaped[nextFreeIndex + 1] = 't';
        nextFreeIndex = nextFreeIndex + 2;
        break;
      case '\\':
        escaped[nextFreeIndex] = '\\';
        escaped[nextFreeIndex + 1] = '\\';
        nextFreeIndex = nextFreeIndex + 2;
        break;
      case '\'':
        escaped[nextFreeIndex] = '\\';
        escaped[nextFreeIndex + 1] = '\'';
        nextFreeIndex = nextFreeIndex + 2;
        break;
      case '\"':
        escaped[nextFreeIndex] = '\\';
        escaped[nextFreeIndex + 1] = '\"';
        nextFreeIndex = nextFreeIndex + 2;
        break;
      default:
        escaped[nextFreeIndex] = letter;
        nextFreeIndex++;
        break; 
    }
  }
  escaped[nextFreeIndex] = '\0';
  return escaped;
}

/* This method replaces escaped characters with their one-character
   equivalents. */
char *unescape(char *escaped) {

  int toMalloc = 0;
  int length = strlen(escaped);
    
  int i;
  char letter;
  
  for (i = 0; i < length; i++) {
    letter = escaped[i];
    
    if (letter == '\\') {
      i++; // skip next letter
    }
    toMalloc ++;
  }
  toMalloc ++; // add space for null
  
  char *unescaped;
  unescaped = malloc(toMalloc*sizeof(char));
  int nextFreeIndex = 0;
  char next;
  
  for (i = 0; i < length; i++) {
    letter = escaped[i];
    
    if (letter == '\\') {
      next = escaped[i + 1];
      switch(next) {  
        case 'n':
          unescaped[nextFreeIndex] = '\n';
          break;
        case 't':
          unescaped[nextFreeIndex] = '\t';
          break;
        case '\\':
          unescaped[nextFreeIndex] = '\\';
          break;
        case '\'':
          unescaped[nextFreeIndex] = '\'';
          break;
        case '\"':
          unescaped[nextFreeIndex] = '\"';
          break;
        default:
          unescaped[nextFreeIndex] = '\\'; // this case really shouldn't happen
          break; 
      }
      i++; // skip next letter
    }
    else {
      unescaped[nextFreeIndex] = letter;
    }
    nextFreeIndex++;
  }
  unescaped[nextFreeIndex] = '\0';
  return unescaped;
}
